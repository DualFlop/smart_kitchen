/*
KitchenLight
04.03.2018  
Danny Bee
*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoOTA.h>
#include <math.h>

const char* ssid = "Gemeinschaftskatze";
const char* password = "Antje0707Daniel1302Soley1210";
const char* mqtt_server = "192.168.0.30";

long lastMsg = 0;
long endtime = 0;
char msg[50];
int value = 0;
int ADCvalue = 0;
int AnalogPin = 1;
int PirPin1=D2;
int LEDPin=D1;
int PIRdetect= 0;
String ADCvalue_str;
char ADCval[50];
int msgOnce=0;
int LightRequest=0;
int LightLevelAnalog=0;
int LightLevelAnalog_old=0;
int LightLevel_old= 0;
int LightLevel= 0;
int MotionPIR=0;
int IntCounterPIR=0;
int UpdateFlag = 0;
double i_HumanEye = 0;

//Change for each Project - Unique name mandatory ************************
String NameOfHost="Kitchenlight";
WiFiClient ClientWiFiKitchen;
PubSubClient client(ClientWiFiKitchen);
// in setup()-------> client.setClient(ClientWiFiKitchen);
// in reconnect() --> if (client.connect("ClientWiFiSteckdose1"))
//************************************************************************



void setup() {
  /**************** Pin Configuration ******************/
  //pinMode(PirPin1, INPUT);  //PIR Sensor Digital Input
  pinMode(LEDPin, OUTPUT);  //PWM Output to P-Chan Gate
  attachInterrupt(digitalPinToInterrupt(PirPin1), IntCallbackPIR, RISING); 
  
  /**************** Serial Connection ******************/  
  Serial.begin(115200);     //Begin Serial Connection
  
  /******************* Wifi Setup **********************/   
  setup_wifi();
  IPAddress mqtt_server(192,168,0,30);
  client.setClient(ClientWiFiKitchen);
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  setup_ota(); 
}


void loop() {

//OTA Update
if(UpdateFlag == 1)
{
    ArduinoOTA.handle();
}

if (!client.connected()) {
    reconnect();
  }
  client.loop();

  MotionHandle();
  LightHandle();
} //END OF LOOP



void LightHandle(void)
{
 if(LightRequest==1)
  {
    if(LightLevelAnalog > LightLevelAnalog_old)
    {
      for(int i=LightLevelAnalog_old; i<=LightLevelAnalog; i++)
      {
        int (i_HumanEye) = exp(i/147.12)-29.6;
        analogWrite(LEDPin, i_HumanEye);
        delayMicroseconds(3000);
        //delay(6);
      }
    }
  
  else if(LightLevelAnalog < LightLevelAnalog_old)
  {
    for(int i=LightLevelAnalog_old; i>=LightLevelAnalog; i--)
    {
     int (i_HumanEye) = exp(i/147.12)-29.6;
     analogWrite(LEDPin, i_HumanEye);
     delayMicroseconds(3000);
    }
  }
  
  LightRequest=0;
  }
}


void MotionHandle(void){
  
  if(MotionPIR)
  {
  PIRdetect = 1;
  MotionPIR = 0;
  endtime=millis()+3000;
    if(msgOnce == 0)
    {
      Serial.println("Detection!");
      client.publish("myhome/Kitchen/PIR", "1");
      msgOnce = 1;
    } 
  }
  
  long nowPIR= millis();
  if((nowPIR > endtime)&&(PIRdetect))
  {
    PIRdetect = 0;
    msgOnce = 0;
    Serial.println("Over");
    client.publish("myhome/Kitchen/PIR", "0");
  }
}

void IntCallbackPIR(){
  if(MotionPIR==0)
  IntCounterPIR++;

  if(IntCounterPIR>2)
  {
  MotionPIR=1;
  
  IntCounterPIR=0;
  Serial.print("Stamp(ms): ");
  Serial.println(millis());
  }
}


