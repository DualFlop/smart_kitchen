
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
   
  payload[length] = '\0';
  String mqttPayload = String((char*)payload);
  String mqttTopic = String((char*)topic);
  String myString = (char*)payload;
  int number = mqttPayload.toInt();

  
  if (strcmp(topic,"myhome/Kitchen/Light")==0)
  {
    payload[length] = '\0';
    String s = String((char*)payload);
    LightLevel_old = LightLevel;
    LightLevel = s.toInt();
    if (LightLevel >100)
    LightLevel = 100;
    if (LightLevel <0)
    LightLevel = 0;
  
    LightLevelAnalog = map(LightLevel, 0, 100, 500, 1024);
    LightLevelAnalog_old = map(LightLevel_old, 0, 100, 500, 1024);
    
    LightRequest=1;
    Serial.println(LightLevelAnalog);
  }


  if (strcmp(topic,"myhome/Kitchen/Update")==0)
  {
     Serial.println("Update Topic");
     if((char)payload[0]== '1'){
       UpdateFlag = 1;
       client.publish("myhome/Kitchen/Status", "Update enabled");
       Serial.println("Update enabled");
     }
     if((char)payload[0]== '0'){
      UpdateFlag = 0;
      client.publish("myhome/Kitchen/Status", "Update disabled");
      Serial.println("Update disabled");
     }
  }
}//END OF CALLBACK

